FROM node:14-alpine as builder
WORKDIR /app
ARG NPM_TOKEN
COPY . .
RUN npm install
RUN npm install -g @nestjs/cli typescript
RUN nest build
RUN npm prune --production


FROM node:14-alpine AS production
WORKDIR /app
COPY --from=builder /app/dist ./dist
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/config ./config
EXPOSE 3000
CMD ["node", "dist/main.js"]
