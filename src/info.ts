import { Scenes, Telegraf } from 'telegraf'
import { Connection, Repository } from 'typeorm'
import { InfoCommand } from './info-command.entity'
import { sendAndRemove } from './message-utils'

export class InfoModule {
  private readonly repository: Repository<InfoCommand>
  private readonly bot: Telegraf<Scenes.SceneContext>
  private readonly addCommandScene = new Scenes.WizardScene<Scenes.WizardContext>(
    'addCommand',
    async (ctx) => {
      console.log(ctx.message['text'])
      ctx.wizard.state['commandInfo'] = {}
      if (!/^[a-zA-Z]{3,20}$/.test(ctx.message['text'])) {
        await ctx.reply(
          'Introduce un nombre de tres a veinte letras sin espacios',
        )
        // await ctx.scene.leave()
        return
      }
      ctx.wizard.state['commandInfo'].command = ctx.message['text']
      await ctx.reply('Introduce el contenido del comando')
      return ctx.wizard.next()
    },
    async (ctx) => {
      if (!ctx.message['text']) {
        await ctx.reply(
          'No se ha detectado ningun texto, vuelve a introducirlo',
        )
        return
      }
      ctx.wizard.state['commandInfo'].text = ctx.message['text']
      const { commandInfo } = ctx.wizard.state as { commandInfo: InfoCommand }
      this.bot.command(commandInfo.command, async (ctx) => {
        const id = ctx.from.id
        await this.bot.telegram.sendMessage(id, commandInfo.text)
      })
      await this.repository.save(commandInfo)
      await ctx.reply('Comando guardado con exito.')
      await ctx.scene.leave()
    },
  )

  constructor(bot: Telegraf<Scenes.SceneContext>, connection: Connection) {
    this.bot = bot
    this.repository = connection.getRepository(InfoCommand)
    this.setCommands().catch((err) =>
      console.log('Error setting info commands: ', err.message),
    )
  }

  async getCommands(): Promise<InfoCommand[]> {
    return await this.repository.find()
  }

  async setCommands(): Promise<void> {
    const commands = await this.repository.find()
    for (const info of commands) {
      this.bot.command(info.command, async (ctx) => {
        const id = ctx.from.id
        try {
          await this.bot.telegram.sendMessage(id, info.text)
        } catch (err) {
          if (err.response.error_code === 403) {
            const message =
              'Debes iniciar el bot por chat privado antes de intentar emplear los comandos.'
            await sendAndRemove(ctx, message)
          }
        }
      })
    }

    this.bot.command('add', async (ctx) => {
      if (!ctx.state.isPrivate) {
        const message =
          'Los comandos solo se pueden crear desde conversaciones privadas.'
        await sendAndRemove(ctx, message)
        return
      }
      if (!ctx.state.isAdmin) {
        await ctx.reply('Solo un administrador puede añadir comandos.')
        return
      }
      await ctx.reply('¿Cual será el nombre del comando?')
      await ctx.scene.enter('addCommand')
    })

    this.bot.command('del', async (ctx) => {
      if (!ctx.state.isAdmin) {
        await sendAndRemove(ctx, 'Solo un administrador puede borrar comandos.')
        return
      }
      const command = ctx.message.text.replace(/\s+/g, ' ').split(' ')[1]
      if (!command) {
        await sendAndRemove(
          ctx,
          'Introduce el nombre del comando con el mensaje.',
        )
        return
      }
      try {
        const commandToDelete = await this.repository.findOne({ command })
        if (!commandToDelete) {
          await sendAndRemove(
            ctx,
            'Comando no encontrado, asegurate de que exista.',
          )
          return
        }
        await this.repository.remove(commandToDelete)
        this.bot.command(command, async (ctx) => {
          await sendAndRemove(
            ctx,
            'Comando eliminado, dejara de funcionar en el proximo reinicio',
          )
        })
      } catch (err) {
        console.log('Failed to remove command')
      }
    })
    this.bot.command('lista', async (ctx) => {
      await ctx.deleteMessage()
      const commands = await this.getCommands()
      const commandList = commands.reduce(
        (actualList, command) => (actualList += `\\${command.command}\n`),
        '',
      )
      const message = `Lista de comandos disponibles:\n${commandList}`
      const id = ctx.from.id
      try {
        await this.bot.telegram.sendMessage(id, message)
        if (!ctx.state.isPrivate) {
          await ctx.deleteMessage()
        }
      } catch (err) {
        if (err.response.error_code === 403) {
          const message =
            'Debes iniciar el bot por chat privado antes de intentar listar los comandos.'
          await sendAndRemove(ctx, message)
        }
      }
    })
  }

  getScenes(): Scenes.BaseScene<Scenes.SceneContext>[] {
    return [this.addCommandScene]
  }
}
