import { Context } from 'telegraf'

const defaultMsgRemoval = +process.env.DEFAULT_MSG_REMOVAL

export async function sendAndRemove(
  ctx: Context,
  message: string,
  seconds = defaultMsgRemoval,
): Promise<void> {
  const sent = await ctx.reply(message)
  setTimeout(async () => {
    try {
      await ctx.deleteMessage()
      await ctx.deleteMessage(sent.message_id)
    } catch (err) {
      console.log(`Failed to remove messages: ${err.message}`)
    }
  }, seconds * 1000)
}
