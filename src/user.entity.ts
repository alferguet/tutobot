import { ChatFromGetChat } from 'typegram'
import { Column, Entity, PrimaryColumn } from 'typeorm'

@Entity('users')
export class User {
  @PrimaryColumn({ type: 'int' })
  uid: number

  @Column({ type: 'boolean' })
  isAdmin: boolean

  constructor(chat?: ChatFromGetChat) {
    if (chat) {
      this.uid = chat.id
      this.isAdmin = true
    }
  }
}
