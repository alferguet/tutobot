import { Scenes, Telegraf } from 'telegraf'
import { Connection, Repository } from 'typeorm'
import { sendAndRemove } from './message-utils'
import { User } from './user.entity'

export class AuthModule {
  private readonly repository: Repository<User>
  private readonly OWNER_UID = parseInt(process.env.OWNER_UID)
  private readonly bot: Telegraf<Scenes.SceneContext>

  constructor(bot: Telegraf<Scenes.SceneContext>, connection: Connection) {
    this.repository = connection.getRepository(User)
    this.bot = bot
    this.bot.use(async (ctx, next) => {
      ctx.state.isAdmin = await this.isAdmin(ctx.from.id)
      ctx.state.isPrivate = ctx.chat.type === 'private'
      return next()
    })
    this.setCommands().catch((err) =>
      console.log(`Error setting admin commands: ${err.message}`),
    )
  }

  async isAdmin(uid: number): Promise<boolean> {
    if (this.OWNER_UID === uid) return true
    const users = await this.repository.find()
    return users.some((user) => user.uid === uid && user.isAdmin)
  }

  async setCommands(): Promise<void> {
    this.bot.command('addAdmin', async (ctx) => {
      if (!ctx.state.isAdmin) {
        await sendAndRemove(
          ctx,
          'Solo un administrador puede añadir otros administradores.',
        )
        return
      }
      const uid = +ctx.message.text.replace(/\s+/g, ' ').split(' ')[1]
      console.log(uid)
      if (!uid) {
        await sendAndRemove(
          ctx,
          'El id de usuario tiene que ser un numero valido.',
        )
        return
      }
      try {
        await this.repository.save({ uid, isAdmin: true })
      } catch (err) {
        console.log(err.message)
        await sendAndRemove(ctx, 'Fallo al guardar los permisos del usuario.')
      }
      await sendAndRemove(ctx, 'Administrador guardado, a currar.')
    })
  }

  getScenes(): Scenes.BaseScene<Scenes.SceneContext>[] {
    return []
  }
}
