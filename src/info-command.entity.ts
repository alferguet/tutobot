import { Column, Entity, PrimaryColumn } from 'typeorm'

@Entity('infocommands')
export class InfoCommand {
  @PrimaryColumn({ type: 'varchar' })
  command: string

  @Column({ type: 'varchar' })
  text: string
}
