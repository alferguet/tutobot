import { config } from 'dotenv'
config()
import 'reflect-metadata'
import { Scenes, session, Telegraf } from 'telegraf'
import { Connection, createConnection } from 'typeorm'
import { AuthModule } from './admin'
import { InfoModule } from './info'
import { InfoCommand } from './info-command.entity'
import { User } from './user.entity'
let connection: Connection
async function initBot() {
  await initConnection()
  const bot = new Telegraf<Scenes.SceneContext>(process.env.BOT_TOKEN)
  const infoModule = new InfoModule(bot, connection)
  const authModule = new AuthModule(bot, connection)
  const scenes = await initScenes(infoModule, authModule)
  const stage = new Scenes.Stage<Scenes.SceneContext>(scenes)
  bot.use(session())
  bot.use(stage.middleware())
  setStartMessage(bot)
  setHelp(bot)
  await bot.launch()
  console.log('Bot launched')
}

async function initConnection() {
  connection = await createConnection({
    type: 'sqlite',
    database: 'db.sql',
    entities: [InfoCommand, User],
    synchronize: true,
  })
}

async function initScenes(
  infoModule: InfoModule,
  adminModule: AuthModule,
): Promise<any> {
  const scenes: Scenes.BaseScene<Scenes.SceneContext>[] = []
  scenes.push(...infoModule.getScenes())
  scenes.push(...adminModule.getScenes())
  return scenes
}

function setStartMessage(bot: Telegraf<Scenes.SceneContext>): void {
  bot.start(async (ctx) => {
    try {
      await ctx.reply(
        'Bienvenido al bot tutorial de teclados mecánicos, introduce un comando para leer la información.',
      )
    } catch (err) {
      console.log(`Some user failed to start the bot: ${err.message}`)
    }
  })
}

function setHelp(bot: Telegraf<Scenes.SceneContext>): void {
  try {
    bot.help(async (ctx) => ctx.reply('Menú de ayuda en construcción.'))
  } catch (err) {
    console.log(err)
  }
}

initBot().catch((err) => console.log(err))
